package com.gt.util;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

public class EmailSender implements Runnable{
	private static ExecutorService service = Executors.newSingleThreadExecutor();
	final static String DEFAULT_USER = "greenwichtechnology2015";
	final static String DEFAULT_PASSWORD = "mattboristony";	
	public final static String DEFAULT_FROM_EMAIL = DEFAULT_USER + "@gmail.com";
	private static final String SWITCH_FILE_NAME = "disableEmailSender";	
    static String DEFAULT_HOST="smtp.gmail.com";
    
	private final String username = DEFAULT_USER;
	private final String password = DEFAULT_PASSWORD;
	
    private static Logger log = Logger.getLogger(EmailSender.class);
    
    private MimeMessage message;
    private Multipart multipart = new MimeMultipart();
	private volatile List<File> tempAttachmentFiles;

	public static void stop(){		
		try {
			log.info("stop, wait up to 15 sec for completion...");
			service.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			log.error( "stop, exception, ignore", e);
		}
	}
	
    /**
     * Creates a new email message without sending it out.
     * @param to destination address
     * @param from source address - must be a valid email address
     * @param subject subject line for this email
     * @param text body of the message
     */
    public EmailSender(String to, String subject, String text) throws Exception{
        Properties props = System.getProperties();
		props.put("mail.smtp.host", DEFAULT_HOST);
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.debug", "true");
        
        log.info("Create message to: "+to+", subject: "+subject+", text: "+text);
		Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });
        message = new MimeMessage(session);
        message.setFrom(new InternetAddress(DEFAULT_FROM_EMAIL));
        for(String t : to.split(",")){
        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(t.trim()));
        }
        message.setSubject(subject);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(text);
        multipart.addBodyPart(messageBodyPart);
        message.setContent(multipart);
    }

    /**
     * Adds file attachment.
     */
    public void attach(File file, String attachmentName) throws Exception{
        log.info("attach: "+file+" as: "+attachmentName);
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(file);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(attachmentName);
        multipart.addBodyPart(messageBodyPart);
    }

	public void attach(byte[] data, String attachmentName) throws Exception{
		log.info("attach: "+attachmentName);
		File file=File.createTempFile("",".tmp",new File(System.getProperty("java.io.tmpdir")));
		file.deleteOnExit();
		if(tempAttachmentFiles==null){
			tempAttachmentFiles=new LinkedList<File>();
			tempAttachmentFiles.add(file);
		}
		OutputStream os=new FileOutputStream(file);
		os.write(data);
		os.flush();
		os.close();
		attach(file,attachmentName);
    }

	public void sendAsync(){
		service.submit(this);
	}
	
    /**
     * Sends message.
     */
    public void send() throws Exception{
		try{
			if(new File(SWITCH_FILE_NAME).exists()){
				log.info("DISABLED: skip sending: " + this);
			}
			else{
				log.info("send:");
				Transport.send(message);
				log.info("send: done");
			}
		}
		catch(Exception e){
			log.error("exception sending email", e);
		}
		finally{
			if(tempAttachmentFiles!=null){
				for(File f : tempAttachmentFiles){
					if(f!=null && f.exists()){
						f.delete();
					}
				}
			}
		}
    }

	@Override
	public void run() {
    	try{
    		send();
    	}
    	catch(Exception e){
    		log.error("exception sending "+ this, e);
    	}
	}
    
    /**
     * Sets SMTP a new host server for all subsequent outgoing messages.
     *  Use it if you need to change default server only.
     */
    public static void setSmtpServer(String host){
        log.info("set new SMTP host: "+host);
        if(host==null)
            throw new NullPointerException("SMTP server is not allowed to be null");
        EmailSender.DEFAULT_HOST=host;
    }
    
    

    @Override
	public String toString() {
		return "EmailSender ["
				+ (username != null ? "username=" + username + ", " : "")
				+ (password != null ? "password=" + password + ", " : "")
				+ (message != null ? "message=" + message + ", " : "")
				+ (multipart != null ? "multipart=" + multipart + ", " : "")
				+ (tempAttachmentFiles != null ? "tempAttachmentFiles="
						+ tempAttachmentFiles : "") + "]";
	}

	/**
     * Returns current SMTP host server.
     */
    public static String getSmtpServer() {return DEFAULT_HOST;}

     /**
      * Unit test demonstrating use of this class and testing it.
      */
    public static void main(String[] args) throws Exception{
//    	ExecutorService executorService = Executors.newFixedThreadPool(10);
//
//    	executorService.execute(new Runnable() {
//    	    public void run() {
//    	        System.out.println("Asynchronous task");
//    	    }
//    	});
//    	executorService.shutdown();
        if(args.length<3){
            System.err.println("Usage: java EmailSender to subject text_without_spaces [attachment_file]\r\n"+
                "Example: java EmailSender blah@yahoo.com test_subject test_text_message test.zip");
            return;
        }
        String to=args[0];
        String subject=args[1];
        String text=args[2];
        EmailSender sender=new EmailSender(to,subject,text);        
        log.info("Sending email:\r\n\tTo: "+to+"\r\n\tSubject: "+subject+"\r\n\tText: "+text+
                "\r\n\tSMTP server: "+getSmtpServer());
        if(args.length>3){
            String file=args[3];
            log.info("\tAttachment file: "+file);
            sender.attach(new File(file),file);
        } 	
        sender.sendAsync();
        log.info("waiting for serice completion");
 //       executorService.awaitTermination(15, TimeUnit.SECONDS);
        EmailSender.stop();
        log.info("Message sent out or failed...");
        System.exit(0);
    }
}
